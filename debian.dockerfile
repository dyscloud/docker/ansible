# syntax=docker/dockerfile:1

ARG ANSIBLE_VERSION

# Use the latest stable Debian slim version
FROM debian:stable-slim AS builder

ARG ANSIBLE_VERSION

# Build environment variables configuration
ENV DEBIAN_FRONTEND=noninteractive \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PIP_NO_CACHE_DIR=1

# Install dependencies and Ansible in a virtual environment
RUN set -ex \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        python3-minimal \
        python3-pip \
        python3-venv \
    && python3 -m venv /usr/local \
    && . /usr/local/bin/activate \
    && pip3 install --no-cache-dir --upgrade -U \
        pip \
        setuptools \
        wheel \
        ansible==${ANSIBLE_VERSION} \
        botocore \
        boto3 \
        jmespath \
    && find /usr/local -type d -name '__pycache__' -exec rm -rf {} + \
    && find /usr/local -type f -name '*.py[co]' -exec rm -f {} +

# Final production image
FROM debian:stable-slim

ARG ANSIBLE_VERSION

# Image metadata
LABEL maintainer="Dyscloud" \
      description="Minimal and secure Ansible image" \
      version="${ANSIBLE_VERSION}" \
      org.opencontainers.image.source="https://gitlab.com/dyscloud/docker/ansible"

# Runtime environment configuration
ENV DEBIAN_FRONTEND=noninteractive \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1

# Copy virtual environment from builder stage
COPY --from=builder /usr/local /usr/local
COPY ansible.cfg /etc/ansible/ansible.cfg

# Install required minimal dependencies
RUN set -ex \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        python3-minimal \
        python3-distutils \
        python3-lib2to3 \
        openssh-client \
        sshpass \
        ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Add healthcheck
HEALTHCHECK --interval=30s --timeout=10s --start-period=5s --retries=3 \
    CMD ansible --version || exit 1

# Default command (optional)
CMD ["ansible", "--version"]
