ARG ANSIBLE_VERSION

FROM python:3-alpine AS builder
ARG ANSIBLE_VERSION

# Install build dependencies and create virtual environment
RUN apk add --no-cache --virtual .build-deps \
        gcc \
        musl-dev \
        python3-dev \
        libffi-dev \
        openssl-dev \
    && python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# Install Python packages in virtual environment
RUN pip install --no-cache-dir --upgrade -U \
        pip \
        setuptools \
        wheel \
        ansible==${ANSIBLE_VERSION} \
        botocore \
        boto3 \
        jmespath \
    && find /opt/venv -type d -name "__pycache__" -exec rm -rf {} + || true \
    && apk del .build-deps

# Final stage
FROM python:3-alpine

# Copy virtual environment from builder
COPY --from=builder /opt/venv /opt/venv
COPY ansible.cfg /etc/ansible/ansible.cfg
ENV PATH="/opt/venv/bin:$PATH"

# Install runtime dependencies only
RUN apk add --no-cache \
        bash \
        sshpass \
        openssh-client \
        git \
        ca-certificates \
    && find /var/log -maxdepth 2 -type f -exec truncate -s 0 {} \; \
    && chown -R ansible:ansible /opt/venv \
    && ansible --version

COPY ansible.cfg /etc/ansible/ansible.cfg

CMD ["ansible-playbook", "$@"]